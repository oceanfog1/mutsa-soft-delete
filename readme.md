# Soft Delete Example

댓글(Comment), 좋아요(Like)가 있는 글(Post)을 지웠(Delete)을 때 Comment, Like등은 지워지지 않고 Post만 지워지는 기능 입니다.


- Post 삭제 시, 해당 포스트에 관련되어있는 Comment, Like도 같이 삭제 되어야합니다.



## 지워진 Record안보이게 하기

아래와 같이 deleted_at is NULL을 Where조건에 추가하여 안보이도록 설정 할 수 있습니다.

```java
@Where(clause = "deleted_at is NULL")
```

