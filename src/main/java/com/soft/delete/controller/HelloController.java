package com.soft.delete.controller;

import com.soft.delete.domain.dto.Response;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/hello")
public class HelloController {

    @GetMapping
    public Response<String> hello() {
        return Response.success("hello");
    }

    @GetMapping("/api-auth-test")
    public Response<String> apiAuthTest() {
        return Response.success("auth test success");
    }
}
