package com.soft.delete.controller;

import com.soft.delete.domain.dto.*;
import com.soft.delete.domain.dto.request.CommentWriteRequest;
import com.soft.delete.domain.dto.response.CommentModifyResponse;
import com.soft.delete.domain.dto.response.CommentWirteResponse;
import com.soft.delete.domain.entity.CommentEntity;
import com.soft.delete.service.PostService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/posts")
public class PostController {

    private final PostService postService;


    @PostMapping
    public Response<PostResponse> posts(@RequestBody PostCreateRequest dto, Authentication authentication){
        System.out.println("Controller Test Enter");
        PostDto postDto = postService.createPost(dto.getTitle(),dto.getBody(), authentication.getName());
        System.out.println("Controller Test");
        return Response.success(new PostResponse("포스트 등록 완료", postDto.getId()));
    }

    @PostMapping("/{postId}/comments")
    public Response<CommentWirteResponse> posts(@PathVariable Integer postId, @RequestBody CommentWriteRequest request, Authentication authentication){
        CommentEntity commentEntity = postService.createComment(request.getComment(), postId);
        return Response.success(CommentWirteResponse.builder()
                        .id(commentEntity.getId())
                        .postId(commentEntity.getPost().getId())
                        .comment(commentEntity.getComment())
                        .userName(commentEntity.getUser().getUserName())
                        .createdAt(commentEntity.getCreatedAt())
                .build());
    }

    @PutMapping("/{postId}/comments/{id}")
    public Response<CommentModifyResponse> commentModify() {
        return Response.success(new CommentModifyResponse());
    }

}
