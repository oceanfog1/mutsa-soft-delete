package com.soft.delete.domain;

public enum UserRole {
    USER, ADMIN
}
