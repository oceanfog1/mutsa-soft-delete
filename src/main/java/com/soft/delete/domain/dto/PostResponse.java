package com.soft.delete.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public class PostResponse {
    private String message;
    private Integer postId;
}
