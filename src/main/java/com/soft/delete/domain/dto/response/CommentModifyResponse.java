package com.soft.delete.domain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class CommentModifyResponse {
    private Integer id;
    private String userName;
    private String comment;
    private Integer postId;
    private LocalDateTime createdAt;
    private LocalDateTime lastModifiedAt;
}
