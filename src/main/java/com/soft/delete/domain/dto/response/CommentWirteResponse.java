package com.soft.delete.domain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentWirteResponse {
    private Integer id;
    private Integer postId;
    private String comment;
    private String userName;
    private LocalDateTime createdAt;
}
