package com.soft.delete.repository;

import com.soft.delete.domain.entity.LikeEntity;
import com.soft.delete.domain.entity.PostEntity;
import com.soft.delete.domain.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface LikeRepository extends JpaRepository<LikeEntity, Integer> {

    Optional<LikeEntity> findByUserAndPost(UserEntity user, PostEntity post);


    @Transactional
    @Modifying
    void deleteAllByPost(PostEntity postEntity);
}
