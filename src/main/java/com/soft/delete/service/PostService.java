package com.soft.delete.service;

import com.soft.delete.domain.dto.PostDto;
import com.soft.delete.domain.entity.CommentEntity;
import com.soft.delete.domain.entity.LikeEntity;
import com.soft.delete.domain.entity.PostEntity;
import com.soft.delete.domain.entity.UserEntity;
import com.soft.delete.exception.AppException;
import com.soft.delete.exception.ErrorCode;
import com.soft.delete.repository.LikeRepository;
import com.soft.delete.repository.PostRepository;
import com.soft.delete.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
@AllArgsConstructor
@Slf4j
public class PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final LikeRepository likeRepository;


    public PostDto createPost(String title, String body, String userName) {
        UserEntity userEntity = userRepository.findByUserName(userName)
                .orElseThrow( ()-> new AppException(ErrorCode.USERNAME_NOT_FOUND));
        PostEntity postEntity = PostEntity.of(title, body, userEntity);

        PostEntity savedPost = postRepository.save(postEntity);
        return new PostDto(savedPost.getId());
    }

    @Transactional
    public boolean delete(String userName, Integer postId) {

        System.out.println("Delete Service Test1");
        PostEntity postEntity = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, String.format("postId is %d", postId)));
        log.info("Delete PostEntity");

        UserEntity userEntity = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, String.format("%s not founded", userName)));

        if (!Objects.equals(postEntity.getUser().getUserName(), userName)) {
            throw new AppException(ErrorCode.INVALID_PERMISSION, String.format("user %s has no permission with post %d", userName, postId)); }

        log.info("like를 지웁니다. postId:{}", postEntity.getId());
        likeRepository.deleteAllByPost(postEntity);
        postRepository.delete(postEntity);

        return true;
    }

    public CommentEntity createComment(String comment, Integer postId) {
        return null;
    }

    public CommentEntity updateComment(String comment, Integer commentId){
        return null;
    }


    public Page<PostDto> my(String param1, String param2) {
        return null;
    }

    public LikeEntity like(Integer postId, String userName) {

        PostEntity postEntity = postRepository.findById(postId)
                .orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, String.format("postId is %d", postId)));

        UserEntity userEntity = userRepository.findByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, String.format("%s not founded", userName)));

        // 이미 눌렀는지
        likeRepository.findByUserAndPost(userEntity, postEntity)
                .ifPresent(item -> {
                    throw new AppException(ErrorCode.ALREADY_LIKED, String.format("userName %s는 이미 %d 포스트에 좋아요를 눌렀습니다.",userName, postId));
                });

        return likeRepository.save(LikeEntity.of(userEntity, postEntity));
    }

}
