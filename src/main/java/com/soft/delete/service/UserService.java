package com.soft.delete.service;

import com.soft.delete.domain.User;
import com.soft.delete.domain.entity.UserEntity;
import com.soft.delete.exception.AppException;
import com.soft.delete.exception.ErrorCode;
import com.soft.delete.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    public User loadUserByUsername(String userName) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByUserName(userName)
                .orElseThrow(()->new AppException(ErrorCode.USERNAME_NOT_FOUND,userName+ "이 없습니다." ));

        User user = User.fromEntity(userEntity);

        return user;
    }

    public UserEntity join(String userName, String password) {

        userRepository.findByUserName(userName).ifPresent(
                user -> {
                    throw new AppException(ErrorCode.DUPLICATED_USER_NAME, " "+ userName +"는 이미 있습니다.");
                }
        );
        UserEntity user = UserEntity.of(userName, encoder.encode(password));

        UserEntity savedUser = userRepository.save(user);

        return savedUser;
    }
}
