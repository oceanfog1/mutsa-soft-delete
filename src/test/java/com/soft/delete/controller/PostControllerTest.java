package com.soft.delete.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.soft.delete.domain.dto.PostDto;
import com.soft.delete.domain.dto.request.CommentWriteRequest;
import com.soft.delete.domain.entity.CommentEntity;
import com.soft.delete.fixture.PostEntityFixture;
import com.soft.delete.fixture.UserEntityFixture;
import com.soft.delete.service.PostService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest({PostController.class, HelloController.class})
class PostControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    PostService postService;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    @DisplayName("post 작성 성공")
    @WithMockUser
    void hello() throws Exception {
        mockMvc.perform(get("/api/v1/hello")
                        .with(csrf()))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("post 작성 성공")
    @WithMockUser
    void createPost() throws Exception {

        when(postService.createPost(any(), any(), any())).thenReturn(PostDto.builder()
                .id(0).build());

        mockMvc.perform(post("/api/v1/posts")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("post 삭제 성공 - soft delete")
    void deletePost() {
        // post는 지워지고
        // comment, like에는 fk가 빠지고
    }

    @Test
    @DisplayName("comment 작성 성공")
    @WithMockUser
    void commentCreateSuccess() throws Exception {

        // 요청 작성
        CommentWriteRequest commentWriteRequest = CommentWriteRequest.builder()
                .comment("this is a comment")
                .build();

        // Service mocking - CommentEntity를 return postService.createComment()
        when(postService.createComment(any(), any()))
                .thenReturn(CommentEntity.builder()
                        .id(0)
                        .post(PostEntityFixture.get("kyeongrok", "1123"))
                        .user(UserEntityFixture.get("kyeongrok", "1123"))
                        .comment("this is a comment")
                        .build());

        mockMvc.perform(post("/api/v1/posts/1/comments")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(commentWriteRequest))
                )
                .andDo(print())
                .andExpect(jsonPath("$.result.comment").value("this is a comment"))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("comment 수정 성공")
    @WithMockUser
    void comment_modify_success() throws Exception {
        /*
        요청(Request)
        {
            "comment" : "modify comment"
        }

        응답(Response)
        {
        "resultCode": "SUCCESS",
        "result":{
            "id": 4,
            "comment": "modify comment",
            "userName": "test",
            "postId": 2,
            "createdAt": "2022-12-20T16:15:04.270741",
            "lastModifiedAt": "2022-12-23T16:15:04.270741"
            }
        }
         */

        // PUT /api/v1/posts/{postId}/comments/{id}
        mockMvc.perform(put("/api/v1/posts/1/comments/1")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(jsonPath("$.result.comment").value("this is a comment"))
                .andExpect(status().isOk());


        // Controller를 호출 했을 때 Service를 호출했을때 리턴

    }

    @Test
    @DisplayName("comment 수정 실패1 - <사유>")
    void comment_modify_fail1() {

    }

    @Test
    @DisplayName("comment 수정 실패2 - <사유>")
    void comment_modify_fail2() {

    }
}