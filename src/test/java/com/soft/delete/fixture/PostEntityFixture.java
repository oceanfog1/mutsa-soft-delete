package com.soft.delete.fixture;


import com.soft.delete.domain.entity.PostEntity;

public class PostEntityFixture {
    public static PostEntity get(String userName, String password) {
        PostEntity postEntity = PostEntity.builder()
                .id(1)
                .user(UserEntityFixture.get(userName, password))
                .title("title")
                .body("body")
                .build();
        return postEntity;
    }
}
