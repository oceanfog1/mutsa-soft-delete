package com.soft.delete.fixture;


import com.soft.delete.domain.User;
import com.soft.delete.domain.UserRole;
import com.soft.delete.domain.entity.UserEntity;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;

public class UserEntityFixture {

    public static UserEntity get(String userName, String password) {
        UserEntity entity = new UserEntity();
        entity.setId(1);
        entity.setUserName(userName);
        entity.setPassword(password);
        entity.setRole(UserRole.USER);
        entity.setCreatedAt(LocalDateTime.now());
        return entity;
    }

    public static User getUser(String userName, String password) {
        User entity = new User();
        entity.setId(1);
        entity.setUsername(userName);
        entity.setPassword(password);
        entity.setRole(UserRole.USER);
        entity.setRegisteredAt(Timestamp.from(Instant.now()));
        return entity;
    }
}
