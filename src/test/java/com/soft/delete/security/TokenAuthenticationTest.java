package com.soft.delete.security;


import com.soft.delete.domain.UserRole;
import com.soft.delete.service.UserService;
import com.soft.delete.utils.JwtTokenUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;


import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

// 이 Test는 Security Config를 로드 해야 하기 때문에 SpringBootTest로 진행 합니다.
@SpringBootTest
@AutoConfigureMockMvc
public class TokenAuthenticationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    UserService userService;

    private String secretKey="mutsa.hello.world.class2.hhhh.iiiii";
    //token 생성하는 메서드
    private String generateToken_1hour(String userName, UserRole role){
        //claims 생성
        return JwtTokenUtils.generateAccessToken(userName, secretKey, 1000 * 60 * 60);
    }
    private String generateToken_1msec(String userName, UserRole role){
        return JwtTokenUtils.generateAccessToken(userName, secretKey, 1);
    }

    @Test
    @Transactional
    @DisplayName("jwt 인증 성공")
    void authenticatedUser() throws Exception {

        String userName = "authtestuser";
        // dummy user생성
        userService.join(userName, "1q2w3e4r");

        // token 생성
        String token = JwtTokenUtils.generateAccessToken(userName, secretKey, 1000 * 60 * 60);

        mockMvc.perform(get("/api/v1/hello/api-auth-test")
                    .with(csrf())
                    .header(HttpHeaders.AUTHORIZATION, "Bearer " + token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("auth test success"))
                .andDo(print());
    }
    @Test
    @Transactional
    @DisplayName("jwt 인증 실패 - token을 보내지 않음") // Bearer가 없을 때
    void jwtAuthenticateFail() throws Exception {
        mockMvc.perform(get("/api/v1/auth-test-api")
                    .with(csrf())
                    .header(HttpHeaders.AUTHORIZATION, "Bearer ")) // Bearer가 없을 때
                .andExpect(status().isUnauthorized())
                .andExpect(jsonPath("$.errorCode").value("INVALID_PERMISSION"))
                .andDo(print());
    }
}
