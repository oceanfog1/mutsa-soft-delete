package com.soft.delete.service;

import com.soft.delete.domain.dto.PostDto;
import com.soft.delete.domain.entity.LikeEntity;
import com.soft.delete.domain.entity.PostEntity;
import com.soft.delete.domain.entity.UserEntity;
import com.soft.delete.repository.LikeRepository;
import com.soft.delete.repository.PostRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.util.AssertionErrors.assertNotNull;


@SpringBootTest
//@ActiveProfiles("test") // local db에 row가 있는지 직접 확인 하고 싶을 때 씁니다.
class PostServiceWithSpringTest {

    @Autowired
    PostService postService;

    @Autowired
    UserService userService;

    @Autowired
    PostRepository postRepository;
    @Autowired
    LikeRepository likeRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    @DisplayName("soft delete test")
    void softDeleteWithLikeDelete() {

        // user등록
        UserEntity userEntity = userService.join("kyeongrok", "1234");

        // post등록
        PostDto postDto = postService.createPost("hello", "world", userEntity.getUserName());

        // like누름
        LikeEntity likeEntity = postService.like(postDto.getId(), userEntity.getUserName());

        // like조회
        LikeEntity selectedLike = likeRepository.findById(likeEntity.getId())
                .orElseThrow(() -> new RuntimeException("like가 없습니다."));

        // post삭제
        postService.delete(userEntity.getUserName(), postDto.getId());

        // post선택
        Optional<PostEntity> selectedPostEntity = postRepository.findById(postDto.getId());

        // post는 지워지고 없음
        assertTrue(selectedPostEntity.isEmpty());

        // like의 deletedAt이 updated되었는지 확인
        assertThrows(RuntimeException.class, ()->{
            LikeEntity foundLikeEntity = likeRepository.findById(likeEntity.getId())
                    .orElseThrow(()->new RuntimeException("해당 id의 like가 없습니다. id:"+likeEntity.getId()));
        });
    }
}